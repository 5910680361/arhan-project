require "rails_helper"

RSpec.describe MealsController, type: :routing do

    describe "Routing" do
        it "routes to #most_popular" do
            expect(:get => "/most_popular").to route_to("meals#most_popular")
        end
    end

end